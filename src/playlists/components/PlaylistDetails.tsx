import React from 'react'
import { Playlist } from '../../core/model/Playlist'


interface Props {
  playlist: Playlist
  onEdit(): void
}


export const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  // const playlist = props.playlist
  // const { playlist } = props

  return (
    <div>
      PlaylistDetails
      {/* dl>(dt+dd)*3 */}

      <dl data-playlist-id={playlist.id}>

        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        {/* <dd style={{ color: playlist.public ? 'red' : 'green' }}> */}

        <dd className={playlist.public ? 'text-danger' : 'text-success'}>
          {playlist.public ? 'Yes' : 'No'}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>Edit</button>
    </div>
  )
}
