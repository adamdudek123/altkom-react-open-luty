import React, { useEffect, useState } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlist: Playlist
  onSave(draft: Playlist): void
  onCancel(): void
}


export const PlaylistEditForm = ({ playlist, onSave, onCancel }: Props) => {

  const [name, setName] = useState(playlist.name)
  const [isPublic, setIsPublic] = useState(playlist.public)
  const [description, setDescription] = useState(playlist.description)

  useEffect(() => {
    setName(playlist.name)
    setIsPublic(playlist.public)
    setDescription(playlist.description)
  }, [playlist])

  const save = () => {
    onSave({
      type: 'playlist',
      id: playlist.id,
      name, 
      public: isPublic,
      description,
    })
  }

  return (
    <div>
      {/* <pre>{JSON.stringify({ name, isPublic }, null, 2)}</pre> */}

      {/* .form-group>label{Name:}+input.form-control */}
      <div className="form-group">
        <label>Name:</label>
        <input type="text" className="form-control" onChange={event => setName(event.target.value)} value={name} />
        <span>  {name.length} / 170</span>
      </div>

      {/* .form-group>label>input[type=checkbox]+{Public} */}
      <div className="form-group">
        <label><input type="checkbox" onChange={e => setIsPublic(e.target.checked)} checked={isPublic} /> Public</label>
      </div>

      {/* .form-group>label{Description:}+textarea.form-control */}
      <div className="form-group">
        <label>Description:</label>
        <textarea className="form-control" onChange={e => setDescription(e.target.value)} value={description}></textarea>
      </div>

      <button className="btn btn-success" onClick={save}>Save</button>
      <button className="btn btn-danger" onClick={onCancel}>Cancel</button>
    </div>
  )
}
