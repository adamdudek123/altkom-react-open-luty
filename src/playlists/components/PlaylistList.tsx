import React, { useState } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlists: Playlist[],
  selectedId: Playlist['id'] | null,
  // onSelect: (id: number) => void
  onSelect(id: Playlist['id']): void
}


export const PlaylistList = ({ playlists, selectedId, onSelect }: Props) => {


  // const select = (id: number) => { /* setSelectedId(id) */ }

  return (
    <div>
      {/* .list-group>.list-group-item{Tekst}*3 */}

      <div className="list-group">
        {playlists.map((playlist, index) =>
          <div className={"list-group-item " + (selectedId === playlist.id ? 'active' : '')} key={playlist.id} onClick={() => onSelect(playlist.id)}>
            {index + 1}. {playlist.name}
          </div>)}
      </div>
    </div>
  )
}
