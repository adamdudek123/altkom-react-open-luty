import React, { useContext, useEffect, useState } from "react"
import { useDispatch, useSelector, useStore } from "react-redux"
import { Playlist } from "../../core/model/Playlist"
import { AppState } from "../../store"
import { PlaylistDetails } from "../components/PlaylistDetails"
import { PlaylistEditForm } from "../components/PlaylistEditForm"
import { PlaylistList } from "../components/PlaylistList"

import * as playlistsReducer from '../../core/reducers/playlists.reducer';
import { UserContext } from "../../core/contexts/UserContext"

interface Props {

}

export const PlaylistsViewRedux = (props: Props) => {
  // const { getState, dispatch } = useStore()
  const dispatch = useDispatch()

  const { user } = useContext(UserContext)

  const playlists = useSelector(playlistsReducer.selectPlaylists)
  const selected = useSelector(playlistsReducer.selectSelectedPlaylist)

  const [mode, setMode] = useState<'details' | 'edit' | 'create'>('details')

  useEffect(() => {
    dispatch(playlistsReducer.loadPlaylists())
  }, [])

  const edit = () => { setMode('edit') }
  const cancel = () => { setMode('details') }
  const createNew = () => { setMode('create') }

  const save = (draft: Playlist) => {

    dispatch(playlistsReducer.savePlaylist(user!.id, draft, (saved: any) => {
      setMode('details')
      dispatch(playlistsReducer.playlistsSelect(saved.id))
    }))
  }

  const select = (id: Playlist['id']) => {
    dispatch(playlistsReducer.playlistsSelect(id))
  }

  const emptyPlaylist = ({
    name: '', public: false, description: ''
  } as Playlist)

  return (
    <div>
      <div className="row">

        <div className="col">
          <PlaylistList playlists={playlists} selectedId={selected?.id || null}
            onSelect={select} />

          <button className="btn btn-info float-right mt-2"
            onClick={createNew}>Create New</button>
        </div>

        <div className="col">
          {!selected && <p>Please select playlist</p>}

          {selected && <>

            {mode === 'details' ? <div>
              <PlaylistDetails playlist={selected} onEdit={edit} />
            </div> : null}

            {(mode === 'edit') && <div>
              <PlaylistEditForm playlist={selected} onSave={save} onCancel={cancel} />
            </div>}

          </>}


          {(mode === 'create') && <div>
            <PlaylistEditForm playlist={emptyPlaylist} onSave={save} onCancel={cancel} />
          </div>}

        </div>

      </div>

    </div>
  )
}