import React, { FC, useState } from 'react';
// import logo from './logo.svg';
// import './App.css';
// {/* <img src={logo} className="App-logo" alt="logo" /> */}
import 'bootstrap/dist/css/bootstrap.css';
// import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { SearchView } from './search/containers/SearchView';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { Route, Switch, Redirect, Link, NavLink } from 'react-router-dom'
import { UserWidget } from './core/contexts/UserContext';
import { AlbumDetails } from './search/containers/AlbumDetails';
import { PlaylistsViewRedux } from './playlists/containers/PlaylistsViewRedux';

// function placki() { } // somelib.js
// declare function placki(params: string):void // types.d.ts 

export const MyLink: FC<{ to: string } & React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>> = ({ to, children, ...restProps }) =>
  <a {...restProps} onClick={(e) => { e.stopPropagation(); window.history.pushState('', '', to) }}>
    {children}
  </a>

function App() {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="/">MusicApp</a>

          <button className="navbar-toggler" type="button" onClick={() => setIsOpen(!isOpen)}>
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className={"collapse navbar-collapse " + (isOpen ? 'show' : '')} >
            <ul className="navbar-nav">

              <li className="nav-item">
                <NavLink className="nav-link" to="/search" activeClassName="placki active">Search</NavLink>
                {/* <NavLink className="nav-link" to="/search/123">Search</NavLink> */}
                {/* <a className="nav-link" href="/search">Search</a> */}
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>
            </ul>
            <UserWidget className="ml-auto" />
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">

            <Switch>
              <Redirect path="/" to="/playlists" exact={true} />
              <Route path="/playlists" component={PlaylistsViewRedux} />
              {/* <Route path="/search/123" component={SearchView} /> */}
              <Route path="/search" component={SearchView} />
              <Route path="/albums/:album_id" component={AlbumDetails} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
