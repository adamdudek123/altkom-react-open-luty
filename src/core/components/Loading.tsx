import React, { FC, ReactElement, ReactNode } from 'react'

interface Props {
  isLoading: boolean
  // data: any
  // content():ReactNode
  // content:ReactNode
}

export const Loading: FC<Props> = ({ isLoading, children }) => {
  return (
    <div>
      {isLoading && <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>}

      {!isLoading && children}

      {/* {content()} */}
      {/* {content} */}
    </div>
  )
}
