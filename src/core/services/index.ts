import axios from "axios";
import { AuthService } from "./AuthService";


export const auth = new AuthService(
  'https://accounts.spotify.com/authorize',
  '651456a6581e4f42a548b9f4b9a6bc6e',
  'http://localhost:3000/',
  'token',
  [
    'playlist-modify-public',
    'playlist-modify-private',
    'playlist-read-private',
    'playlist-read-collaborative',
  ],
  true
)



axios.interceptors.request.use((config) => {
  // if (config.headers['X-I_AM_SPCEIAL']) { }
  config.headers['Authorization'] = 'Bearer ' + auth.getToken()
  return config
})

axios.interceptors.response.use((config) => config, error => {
  // console.error(error)

  if (error.response?.data?.error) {
    if (error.response.status === 401) { auth.login() }

    return Promise.reject(error.response.data.error)
  } else {
    return Promise.reject(new Error('Unexpected error'))
  }
})