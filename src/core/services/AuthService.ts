
export class AuthService {
  private token = ''

  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type: string = 'token',
    private scopes: string[] = [],
    private show_dialog = false,
  ) { }

  getToken(){
    return this.token;
  }

  login() {
    const { client_id, response_type, redirect_uri } = this

    const params = new URLSearchParams({
      client_id,
      response_type,
      redirect_uri,
      scope: this.scopes.join(' ')
    })
    this.show_dialog && params.set('show_dialog', 'true')

    const url = `${this.auth_url}?${params}`
    window.location.href =  (url)
  }

  init() {
    this.token = JSON.parse(sessionStorage.getItem('token') || '""');

    if (window.location.hash) {
      const p = new URLSearchParams(window.location.hash.slice(1))
      const token = p.get('access_token') || ''
      if (token) {
        this.token = token;
        sessionStorage.setItem('token', JSON.stringify(this.token))
        window.location.hash = ''
      }
    }

    if (!this.token) { this.login() }
  }
}