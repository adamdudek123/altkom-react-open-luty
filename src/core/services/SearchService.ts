import axios from 'axios'
import { auth } from '.'
import { Album } from '../model/Album'
import { AlbumSearchResponse, ArtistSearchResponse } from '../model/Search'



export const searchAlbums = (query: string) => {
  return axios.get<AlbumSearchResponse>('https://api.spotify.com/v1/search', {
    // headers: { 'X-I_AM_SPCEIAL': true,},
    params: {
      type: 'album', q: query
    }
  }).then(resp => resp.data['albums'].items)
}


export const getAlbumById = async (id: string) => {
  const { data } = await axios.get<Album>('https://api.spotify.com/v1/albums/' + id)
  return data
}

// export const SearchAlbums = (query: string) => {

//   return axios.get('albums.json', {})
//     .then(resp => resp.data)

// }