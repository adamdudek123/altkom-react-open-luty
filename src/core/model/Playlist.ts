import { Track } from "./Track";



export interface Playlist {
  type: 'playlist'
  id: string;
  name: string;
  public: boolean;
  description: string;
  tracks?: Track[]
}


// const x: 'x' | 'y' = 'y'

// export class Playlist implements Playlist {
//   id: string
//   constructor(id: string, name: string, public: boolean, description: string) {
//     this.id = id
//   }

//   toJSON() {
//     return {
//     }
//   }

//   setId() { } getId() { }
// }

// const result: Playlist | Track = {} as any;

// if (result.type == 'playlist') {
//   result.tracks
// } else {
//   result.duration
// }


// interface Something {
//   id: number | string;
// }

// const s: Something = {} as any

// if ('number' === typeof s.id) {
//   s.id.toExponential()
// } else {
//   s.id.blink()
// }

// const p: Playlist = {} as any;

// if (p.tracks) { p.tracks.length }
// const len1 = p.tracks && p.tracks.length
// const len2 = p.tracks ? p.tracks.length : 0
// const len3 = p.tracks?.length
// const len4 = p.tracks!.length 
