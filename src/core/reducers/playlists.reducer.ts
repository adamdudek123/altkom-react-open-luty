import axios from 'axios'
import { Action, Dispatch, Reducer } from 'redux'
import { AppState } from '../../store'
import { Playlist } from '../model/Playlist'
import { PagingObject } from '../model/Search'

export interface PlaylistState {
  items: Playlist[],
  selectedId: Playlist['id'] | null,
  message: string
}

export const featureKey = 'playlists'

export const initialState: PlaylistState = {
  items: [{
    id: '123',
    type: 'playlist',
    name: 'Moja playlista 123',
    public: false,
    description: 'Ulubiona<3'
  },
  {
    id: '234',
    type: 'playlist',
    name: 'Moja playlista 234',
    public: false,
    description: 'Ulubiona<3'
  },
  {
    id: '345',
    type: 'playlist',
    name: 'Moja playlista 345',
    public: false,
    description: 'Ulubiona<3'
  }],
  selectedId: null,
  message: ''
}


export const reducer: Reducer<PlaylistState, Actions> = (state = initialState, action): PlaylistState => {
  switch (action.type) {
    case 'PLAYLISTS_LOAD_SUCCESS': return { ...state, items: action.payload.data }
    case 'PLAYLISTS_LOAD_FAILED': return { ...state, message: action.payload.message }
    case 'PLAYLISTS_CREATE': return { ...state, items: [...state.items, action.payload.draft] }
    case 'PLAYLISTS_SELECTED': return { ...state, selectedId: action.payload.id }
    case 'PLAYLISTS_UPDATED': {
      const draft = action.payload.draft
      return { ...state, items: state.items.map(p => p.id === draft.id ? draft : p), message: 'Playlist updated!' }
    }
    case 'PLAYLISTS_REMOVED': return {
      ...state,
      items: state.items.filter(p => p.id !== action.payload.id), message: 'Playlist removed!'
    }
    default: return state
  }
}

export const playlistsLoadStart = (): PLAYLISTS_LOAD_START => ({
  type: 'PLAYLISTS_LOAD_START', payload: {}
})
export const playlistsLoadSuccess = (data: Playlist[]): PLAYLISTS_LOAD_SUCCESS => ({
  type: 'PLAYLISTS_LOAD_SUCCESS', payload: { data }
})

export const playlistsLoadFailed = (error: Error): PLAYLISTS_LOAD_FAILED => ({
  type: 'PLAYLISTS_LOAD_FAILED', payload: error
})

export const playlistsSelect = (id: Playlist['id']): PLAYLISTS_SELECTED => ({
  type: 'PLAYLISTS_SELECTED', payload: { id }
})

export const playlistsUpdate = (draft: Playlist): PLAYLISTS_UPDATED => ({
  type: 'PLAYLISTS_UPDATED', payload: { draft }
})
export const playlistsCreated = (draft: Playlist): PLAYLISTS_CREATE => ({
  type: 'PLAYLISTS_CREATE', payload: { draft }
})



interface PLAYLISTS_LOAD_START extends Action<'PLAYLISTS_LOAD_START'> {
  payload: {}
}

interface PLAYLISTS_LOAD_SUCCESS extends Action<'PLAYLISTS_LOAD_SUCCESS'> {
  payload: { data: Playlist[] }
}

interface PLAYLISTS_LOAD_FAILED extends Action<'PLAYLISTS_LOAD_FAILED'> {
  payload: Error
}

interface PLAYLISTS_CREATE extends Action<'PLAYLISTS_CREATE'> {
  payload: { draft: Playlist }
}

interface PLAYLISTS_UPDATED extends Action<'PLAYLISTS_UPDATED'> {
  payload: { draft: Playlist }
}

interface PLAYLISTS_SELECTED extends Action<'PLAYLISTS_SELECTED'> {
  payload: { id: Playlist['id'] }
}

interface PLAYLISTS_REMOVED extends Action<'PLAYLISTS_REMOVED'> {
  payload: { id: Playlist['id'] }
}

type Actions =
  | PLAYLISTS_LOAD_SUCCESS
  | PLAYLISTS_LOAD_FAILED
  | PLAYLISTS_CREATE
  | PLAYLISTS_UPDATED
  | PLAYLISTS_SELECTED
  | PLAYLISTS_REMOVED

/* Selectors */
// https://github.com/reduxjs/reselect

export const selectPlaylists = (state: AppState) => state[featureKey].items;
export const selectSelectedPlaylist = (state: AppState) =>
  state[featureKey].items.find(p => p.id === state[featureKey].selectedId)


/* ASYNC ACTION CREATORS */

export const loadPlaylists = () => (dispatch: Dispatch<any>) => {
  dispatch(playlistsLoadStart())

  axios.get<PagingObject<Playlist>>('https://api.spotify.com/v1/me/playlists')
    .then(
      res => dispatch(playlistsLoadSuccess(res.data.items)),
      error => dispatch(playlistsLoadFailed(error))
    )
}

export const savePlaylist = (user_id: string, draft: Playlist, cb: any) => (dispatch: Dispatch<any>) => {
  if (draft.id) {
    return axios.put(`https://api.spotify.com/v1/playlists/${draft.id}`, {
      name: draft.name,
      public: draft.public,
      description: draft.description,
    }).then(res => {
      dispatch(playlistsUpdate(draft))
      cb(draft)
    })
  } else {
    return axios.post(`https://api.spotify.com/v1/users/${user_id}/playlists`, {
      name: draft.name,
      public: draft.public,
      description: draft.description,
    }).then(res => {
      dispatch(playlistsCreated(res.data))
      cb(res.data)
    })
  }

}