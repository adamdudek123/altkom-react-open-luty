import { applyMiddleware, combineReducers, compose, createStore, Middleware, MiddlewareAPI, Reducer } from 'redux';
import * as counter from './core/reducers/counter.reducer';
import * as playlists from './core/reducers/playlists.reducer';

import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'

export interface AppState {
  counter: number;
  nested: {
    counter2: number;
  }
  [playlists.featureKey]: playlists.PlaylistState
}

const reducer = combineReducers<AppState>({
  [counter.featureKey]: counter.counterReducer,
  [playlists.featureKey]: playlists.reducer,

  nested: combineReducers({
    'counter2': counter.counterReducer,
  }),
})


// const logger: Middleware = (api) => (dispatch) => (action) => {
//   dispatch(action)
//   console.log(action, api.getState())
// }

// const thunk: Middleware = (api) => (dispatch) => (action) => {
//   if ('function' === typeof action) {
//     return action(dispatch)
//   } else {
//     return dispatch(action)
//   }
// }

const composeEnhancers =
  typeof window === 'object' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;


export const store = createStore(reducer, composeEnhancers(applyMiddleware(
  logger,
  thunk
)))

  // ; (window as any).inc = inc
  // ; (window as any).dec = dec
  ; (window as any).store = store
  // store.subscribe(()=> console.log(store.getState()) )
  // store.dispatch( inc() )