import React, { useEffect, useReducer, useState } from 'react'
import { Loading } from '../../core/components/Loading'
import { useDataFetchService } from '../../core/hooks/datafetch'
import { Album } from '../../core/model/Album'
import { searchAlbums } from '../../core/services/SearchService'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import { reducer, searchStart, searchSuccess, searchFailed, initialState } from '../reducers/SearchReducer'
import { RouteComponentProps, useLocation, useHistory, useParams } from 'react-router-dom'

const fixtureAlbums: Pick<Album, 'id' | 'name' | 'images'>[] = [
  { id: '123', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], name: 'Album 123' },
  { id: '234', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], name: 'Album 234' },
  { id: '345', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], name: 'Album 345' },
]

const exampleAlbums: Album[] = fixtureAlbums as unknown as Album[]

interface Props extends RouteComponentProps { }

export const SearchView = (props: Props) => {
  const [{ query, results, message, isLoading }, dispatch] = useReducer(reducer, initialState)

  // const params = new URLSearchParams(props.location.search)
  
  const { push } = useHistory()
  const { search: searchPath } = useLocation()
  const params = new URLSearchParams(searchPath)
  const q = params.get('q')

  const changeQuery = async (query: string) => {
    // props.history.push('/search?q=' + query)
    push('/search?q=' + query)
  }

  useEffect(() => {
    if (!q) return

    (async () => {
      try {
        dispatch(searchStart({ query: q }))
        dispatch(searchSuccess({ results: await searchAlbums(q) }))
      } catch (error) {
        dispatch(searchFailed({ error }))
      }
    })()
  }, [q])

  return (
    <div>
      <div className="row">
        <div className="col">
          {query}
          <SearchForm onSearch={changeQuery} query={query} />
        </div>
      </div>
      <div className="row">
        <div className="col">

          <Loading isLoading={isLoading}>
            {message && <p className="alert alert-danger">{message}</p>}

            <SearchResults results={results} />
          </Loading>


        </div>
      </div>
    </div >
  )
}

{/* <GoogleMap>
    results.map(r => <googleMarker></googleMarker>)
</GoogleMap> */}