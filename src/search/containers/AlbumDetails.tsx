import React, { useEffect, useState } from 'react'
import { AlbumCard } from '../components/AlbumCard'

// http://localhost:3000/albums/details/5djDvf7OozECsKzEB04uYg

// http://localhost:3000/albums/details?id=5djDvf7OozECsKzEB04uYg

// https://api.spotify.com/v1/albums/{id}

// get id from params

// show loading 

// fetch album

// show album

// add link in album search results

import { useParams } from 'react-router-dom'
import { Loading } from '../../core/components/Loading'
import { Album } from '../../core/model/Album'
import { getAlbumById } from '../../core/services/SearchService'

interface Props {

}

export const AlbumDetails = (props: Props) => {
  const [message, setMessage] = useState('')
  const [album, setAlbum] = useState<Album | null>(null)
  const { album_id } = useParams<{ album_id: string }>()


  useEffect(() => {
    if (!album_id) { return }
    (async () => {
      try {
        setMessage('')
        setAlbum(await getAlbumById(album_id))
      } catch (err) {
        setMessage(err.message)
      }
    })()
  }, [album_id])

  return (
    <Loading isLoading={!album}>{album && <div>
      <div className="row">
        <div className="col">
          <AlbumCard result={album!} />
        </div>
        <div className="col">
          <h1>{album!.name}</h1>
        </div>
      </div>
    </div>}
    </Loading>
  )
}
