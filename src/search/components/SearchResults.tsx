import React from 'react'
import { Album } from '../../core/model/Album'
import { AlbumCard } from './AlbumCard'

interface Props {
  results: Album[]
}

export const SearchResults = ({results}: Props) => {
  return (
    <div>
      <div className="row row-cols-2 row-cols-sm-4 no-gutters g-0">
        {results.map((result) => <div className="col mb-4" key={result.id}>
          <AlbumCard result={result} />
        </div>)}
      </div>
    </div>
  )
}
