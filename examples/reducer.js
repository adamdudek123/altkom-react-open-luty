reducer = (state, action) => {
  switch(action.type){
      case 'INC': return {...state, counter: state.counter + action.payload };
      case 'DEC': return {...state, counter: state.counter - action.payload };

      case 'TODO': return {...state, todo: [...state.todo, action.payload] };

      default:    return state
  }
}

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload) => ({type:'TODO', payload});

state = {
  counter: 1,
  todo:['placki']
}

state = reducer(state, inc())
state = reducer(state, inc())
state = reducer(state, {type:'unknown'})
state = reducer(state, dec(2))
state = reducer(state, addTodo('zakupy'))
state = reducer(state, addTodo('redux'))
stateold = state;
state = reducer(state, inc())

stateold.todo === state.todo 

// [inc(),inc(),dec(2), addTodo('zakupy'),inc()].reduce(reducer, {
//     counter: 1,
//     todo:['placki']
// })