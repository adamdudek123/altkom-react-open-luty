console.log(1)

setTimeout(()=>console.log(2), 0)

Promise.resolve(3).then(console.log);

now = Date.now()
while( now + 5000 > Date.now() ){ }

console.log(4) 


// ==========
function echo(msg, cb){
  setTimeout(function(){
     cb(msg)
  },2000)
}


echo('Ala', (data) => {
   echo(data + ' ma ', (data) => {
          echo(data + 'kota', (data) => {
       console.log(data)
      }) 
  }) 
}) 

/// ================

function echo(msg){
  return new Promise((resolve)=>{
       setTimeout(function(){
          resolve(msg)
       },2000)
  });
}


p = echo('Ala') 


p.then( res => console.log(res) )
p.then( res => console.log(res) )
p.then( res => console.log(res) )

// ===========


function echo(msg){
  return new Promise((resolve)=>{
       setTimeout(function(){
          resolve(msg)
       },2000)
  });
}

p = echo('Ala') 
p2 = p.then( res => res + ' ma ')
p3 = p2.then( res => echo(res + 'kota!') )
p3.then(console.log) 

console.log('Bob')

// ===========

fetch('/albums.json')

    .then(res => res.ok? res.json() : Promise.reject('error!'))
    .then(console.log, console.error)

// ============
